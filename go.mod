module go-goof

go 1.16

replace golang.org/x/text => golang.org/x/text v0.3.2

require (
	github.com/gin-gonic/gin v1.6.3
	golang.org/x/text v0.3.5
)
